package io.gitlab.jfronny.sdom.ui

import com.intellij.openapi.actionSystem.ActionGroup
import com.intellij.openapi.actionSystem.ActionManager
import org.jetbrains.annotations.NonNls
import javax.swing.Box
import javax.swing.JComponent

class SDActionToolBar(orientation: ToolBarOrientation, place: @NonNls String = "???") {
    private val actionGroup = ActionManager.getInstance()
        .getAction("io.gitlab.jfronny.sdom.actions.SDToolbarActions") as ActionGroup

    private val toolbar = ActionManager.getInstance().createActionToolbar(place, actionGroup, orientation.value)

    fun setTargetComponent(component: JComponent) {
        toolbar.targetComponent = component
    }

    val component: Box
        get() = Box.createHorizontalBox().apply { add(toolbar.component) }
}

enum class ToolBarOrientation(val value: Boolean) {
    HORIZONTAL(true), VERTICAL(false)
}