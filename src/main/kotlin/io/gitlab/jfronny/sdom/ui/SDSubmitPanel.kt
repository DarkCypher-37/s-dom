package io.gitlab.jfronny.sdom.ui

import com.intellij.execution.filters.TextConsoleBuilderFactory
import com.intellij.execution.ui.ConsoleView
import com.intellij.openapi.project.Project
import com.intellij.openapi.ui.ComponentContainer
import com.intellij.openapi.util.Disposer
import io.gitlab.jfronny.sdom.SDom
import io.gitlab.jfronny.sdom.model.Contest
import io.gitlab.jfronny.sdom.model.SDJudgement
import io.gitlab.jfronny.sdom.model.SDProblem
import io.gitlab.jfronny.sdom.util.OutputType
import io.gitlab.jfronny.sdom.util.println
import java.awt.BorderLayout
import javax.swing.JComponent
import javax.swing.JPanel

class SDSubmitPanel (
    project: Project,
) : ComponentContainer {
    private val panel = JPanel(BorderLayout())
    private val console: ConsoleView

    init {
        console = TextConsoleBuilderFactory.getInstance().createBuilder(project).console

        Disposer.register(this, console)

        val splitter = console.component // replace this to add stuff around the console

        val toolWindowPanel = SDToolWindowPanel(
            topComponent = SDActionToolBar(ToolBarOrientation.HORIZONTAL).apply { setTargetComponent(splitter) }.component,
            mainComponent = splitter
        )
        panel.add(toolWindowPanel.getPanel())
        panel.isVisible = true
    }

    fun logSDJudgement(judgement: Result<SDJudgement>, contest: Contest, problem: SDProblem, filename: String) {
        judgement.fold(
            onSuccess = { result ->
                val parsedResult =
                    result.judgementTypeId
                        ?.let { SDom.judgementTypes?.get(it) }
                        ?.let { it.name to it.solved }
                        ?: ("Unknown" to false)
                console.println(
                    "Submission of \"$filename\" to problem \"${problem.name}\", got result: ${parsedResult.first}",
                    if (parsedResult.second) OutputType.SUCCESS else OutputType.ERROR
                )

                if (parsedResult.second) {
                    problem.solved = true
                }
            },
            onFailure = { e ->
                console.println("Judgement failed: ${e.message}", OutputType.ERROR)
            }
        )
        panel.revalidate()
    }

    fun logSubmission(contest: Contest, problem: SDProblem, filename: String) {
        console.println("Submitted file \"$filename\" to problem \"${problem.name}\", Waiting for result...", OutputType.OUTPUT)
        panel.revalidate()
    }

    override fun dispose() {}

    override fun getComponent(): JComponent = panel

    override fun getPreferredFocusableComponent(): JComponent = console.component

}