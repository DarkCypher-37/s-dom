package io.gitlab.jfronny.sdom.ui

import com.intellij.openapi.fileEditor.FileEditor
import com.intellij.openapi.fileEditor.FileEditorLocation
import com.intellij.openapi.fileEditor.FileEditorState
import com.intellij.openapi.util.Disposer
import com.intellij.openapi.util.UserDataHolderBase
import com.intellij.openapi.vfs.VirtualFile
import com.intellij.ui.jcef.JBCefBrowser
import com.intellij.ui.jcef.JBCefBrowserBuilder
import com.intellij.ui.jcef.JBCefClient
import io.gitlab.jfronny.sdom.util.pdfBootstrap
import org.jetbrains.annotations.Nls
import java.beans.PropertyChangeListener
import javax.swing.JComponent

class SDBrowserViewer(private val file: VirtualFile, cefClient: JBCefClient) : UserDataHolderBase(), FileEditor {
    private val browser: JBCefBrowser = JBCefBrowserBuilder().setClient(cefClient)
        .setEnableOpenDevToolsMenuItem(false)
        .build()

    init {
        browser.loadHTML(pdfBootstrap(file.contentsToByteArray()))
    }

    private val component = browser.component

    override fun getComponent(): JComponent {
        return component
    }

    override fun getPreferredFocusedComponent(): JComponent? {
        return component
    }

    override fun getName(): @Nls(capitalization = Nls.Capitalization.Title) String {
        return "S-dom Integrated Statement Viewer"
    }

    override fun setState(fileEditorState: FileEditorState) {
    }

    override fun isModified(): Boolean {
        return false
    }

    override fun isValid(): Boolean {
        return file.isValid
    }

    override fun addPropertyChangeListener(propertyChangeListener: PropertyChangeListener) {
    }

    override fun removePropertyChangeListener(propertyChangeListener: PropertyChangeListener) {
    }

    override fun getCurrentLocation(): FileEditorLocation? {
        return null
    }

    override fun dispose() {
        Disposer.dispose(this)
    }

    override fun getFile(): VirtualFile? {
        return this.file
    }
}