package io.gitlab.jfronny.sdom.ui

import com.intellij.openapi.fileTypes.FileType
import com.intellij.testFramework.LightVirtualFileBase
import com.intellij.util.LocalTimeCounter
import java.io.ByteArrayInputStream
import java.io.ByteArrayOutputStream
import java.io.InputStream
import java.io.OutputStream

class ByteVirtualFile(name: String, fileType: FileType?, modificationStamp: Long, private val content: ByteArray) : LightVirtualFileBase(name, fileType, modificationStamp) {
    constructor() : this("")
    constructor(name: String) : this(name, ByteArray(0))
    constructor(name: String, content: ByteArray) : this(name, null, LocalTimeCounter.currentTime(), content)
    constructor(name: String, fileType: FileType?, content: ByteArray) : this(name, fileType, LocalTimeCounter.currentTime(), content)

    override fun getOutputStream(requestor: Any?, newModificationStamp: Long, newTimeStamp: Long): OutputStream = ByteArrayOutputStream()
    override fun contentsToByteArray(): ByteArray = this.content.copyOf()
    override fun getInputStream(): InputStream = ByteArrayInputStream(content)
    override fun getLength(): Long = content.size.toLong()
}