package io.gitlab.jfronny.sdom.ui

import com.intellij.openapi.ui.DialogWrapper
import com.intellij.ui.dsl.builder.AlignX
import com.intellij.ui.dsl.builder.bindText
import com.intellij.ui.dsl.builder.panel
import io.gitlab.jfronny.sdom.settings.SDCredentials
import javax.swing.JComponent

class SDLoginDialogWrapper : DialogWrapper(true) {
    var username = ""
    var password = ""
    var url = ""

    init {
        title = "Log in to DOMjudge"
        url = SDCredentials.url.trimEnd('/').run { if (endsWith("/api/v4")) this.substring(0, this.length - 7) else this}
        init()
    }

    override fun createCenterPanel(): JComponent = panel {
        row {
            label("Username")
            textField().align(AlignX.FILL).bindText(this@SDLoginDialogWrapper::username)
        }
        row {
            label("Password")
            passwordField().align(AlignX.FILL).bindText(this@SDLoginDialogWrapper::password)
        }
        row {
            label("API URL")
            textField().align(AlignX.FILL).bindText(this@SDLoginDialogWrapper::url)
        }
    }
}