package io.gitlab.jfronny.sdom.actions

import com.intellij.notification.Notification
import com.intellij.notification.NotificationAction
import com.intellij.openapi.actionSystem.*
import io.gitlab.jfronny.sdom.SDom
import io.gitlab.jfronny.sdom.ui.SDLoginDialogWrapper
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

class SDLoginAction(text: String) : NotificationAction(text) {
    override fun getActionUpdateThread(): ActionUpdateThread = ActionUpdateThread.BGT

    constructor() : this("")

    override fun actionPerformed(e: AnActionEvent, notification: Notification) = actionPerformed(e)
    override fun actionPerformed(e: AnActionEvent) {
        val dialogWrapper = SDLoginDialogWrapper()

        if (dialogWrapper.showAndGet()) {
            CoroutineScope(Job() + Dispatchers.IO).launch {
                SDom.login(username = dialogWrapper.username, password = dialogWrapper.password, url = dialogWrapper.url)
            }
        }
    }

    companion object {
        fun perform() {
            SDLoginAction().actionPerformed(
                AnActionEvent(
                    null,
                    DataContext.EMPTY_CONTEXT,
                    ActionPlaces.UNKNOWN,
                    Presentation(),
                    ActionManager.getInstance(),
                    0
                )
            )
        }
    }
}