package io.gitlab.jfronny.sdom.actions

import com.intellij.notification.Notification
import com.intellij.notification.NotificationAction
import com.intellij.openapi.actionSystem.*
import com.intellij.openapi.actionSystem.ex.ComboBoxAction
import com.intellij.openapi.project.DumbAware
import com.intellij.openapi.project.DumbAwareAction
import com.intellij.openapi.ui.popup.JBPopup
import com.intellij.openapi.ui.popup.JBPopupFactory
import com.intellij.openapi.util.Condition
import io.gitlab.jfronny.sdom.SDom
import io.gitlab.jfronny.sdom.model.Contest
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import java.awt.Dimension
import javax.swing.JComponent

class SDContestSelectionComboBoxAction : ComboBoxAction(), DumbAware {
    override fun getActionUpdateThread(): ActionUpdateThread = ActionUpdateThread.BGT

    override fun update(e: AnActionEvent) {
        e.presentation.isEnabledAndVisible = SDom.loggedIn
        e.presentation.setText(SDom.currentContest?.name ?: "Select Contest", false)
    }

    override fun createPopupActionGroup(button: JComponent, dataContext: DataContext): DefaultActionGroup {
        val actionGroup = DefaultActionGroup()

        actionGroup.addAll(SDom.contests.map { SelectContestAction(it, it == SDom.currentContest) })

        return actionGroup
    }

    override fun createActionPopup(
        group: DefaultActionGroup,
        context: DataContext,
        disposeCallback: Runnable?
    ): JBPopup {
        val popup = JBPopupFactory.getInstance().createActionGroupPopup(
            myPopupTitle,
            group,
            context,
            null,
            shouldShowDisabledActions(),
            disposeCallback,
            maxRows,
            preselectCondition,
            null
        )
        popup.setMinimumSize(Dimension(minWidth, minHeight))
        return popup
    }

    override fun getPreselectCondition(): Condition<AnAction> =
        Condition<AnAction> { if (it is SelectContestAction) it.isSelected else false }

    override fun shouldShowDisabledActions(): Boolean = true

    private class SelectContestAction(
        private val contest: Contest,
        val isSelected: Boolean
    ) : DumbAwareAction() {
        init {
            val name = contest.name
            templatePresentation.setText(name, false)
        }

        override fun getActionUpdateThread(): ActionUpdateThread = ActionUpdateThread.BGT

        override fun actionPerformed(e: AnActionEvent) {
            CoroutineScope(Job() + Dispatchers.IO).launch {
                SDom.selectContest(contest, e.project)
            }
        }
    }
}

class SDContestSelectionNotificationAction(text: String) : NotificationAction(text) {
    constructor() : this("")

    private val selectionAction = SDContestSelectionComboBoxAction()
    override fun actionPerformed(e: AnActionEvent, notification: Notification) = selectionAction.actionPerformed(e)
}