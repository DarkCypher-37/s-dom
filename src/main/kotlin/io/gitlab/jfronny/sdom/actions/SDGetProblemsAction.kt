package io.gitlab.jfronny.sdom.actions

import com.intellij.openapi.actionSystem.ActionUpdateThread
import com.intellij.openapi.actionSystem.AnActionEvent
import com.intellij.openapi.project.DumbAwareAction
import io.gitlab.jfronny.sdom.SDom
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

class SDGetProblemsAction : DumbAwareAction() {
    override fun getActionUpdateThread(): ActionUpdateThread = ActionUpdateThread.BGT

    override fun update(e: AnActionEvent) {
        e.presentation.isVisible = SDom.loggedIn
        e.presentation.isEnabled = SDom.loggedIn && SDom.currentContest != null
    }

    override fun actionPerformed(e: AnActionEvent) {
        CoroutineScope(Job() + Dispatchers.IO).launch {
            SDom.loadProblems()
        }
    }
}