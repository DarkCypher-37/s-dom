package io.gitlab.jfronny.sdom.actions

import com.intellij.notification.Notification
import com.intellij.notification.NotificationAction
import com.intellij.openapi.actionSystem.*
import com.intellij.openapi.actionSystem.ex.ComboBoxAction
import com.intellij.openapi.project.DumbAware
import com.intellij.openapi.project.DumbAwareAction
import com.intellij.openapi.ui.popup.JBPopup
import com.intellij.openapi.ui.popup.JBPopupFactory
import com.intellij.openapi.util.Condition
import com.intellij.util.ui.ColorIcon
import io.gitlab.jfronny.sdom.SDom
import io.gitlab.jfronny.sdom.model.SDProblem
import java.awt.Color
import java.awt.Dimension
import javax.swing.JComponent

class SDProblemSelectionComboBoxAction : ComboBoxAction(), DumbAware {
    override fun getActionUpdateThread(): ActionUpdateThread = ActionUpdateThread.BGT

    override fun update(e: AnActionEvent) {
        e.presentation.isVisible = SDom.loggedIn
        e.presentation.isEnabled = SDom.loggedIn && SDom.currentContest != null
        e.presentation.setText(SDom.currentProblem?.name ?: "Select Problem", false)
    }

    override fun createPopupActionGroup(button: JComponent, dataContext: DataContext): DefaultActionGroup {
        val actionGroup = DefaultActionGroup()

        actionGroup.addAll(
            SDom.problems
                ?.values
                ?.sorted()
                ?.map { SelectProblemAction(it, it == SDom.currentProblem) }
                ?: emptyList()
        )

        return actionGroup
    }

    override fun createActionPopup(
        group: DefaultActionGroup,
        context: DataContext,
        disposeCallback: Runnable?
    ): JBPopup {
        val popup = JBPopupFactory.getInstance().createActionGroupPopup(
            myPopupTitle,
            group,
            context,
            null,
            shouldShowDisabledActions(),
            disposeCallback,
            maxRows,
            preselectCondition,
            null
        )
        popup.setMinimumSize(Dimension(minWidth, minHeight))
        return popup
    }

    override fun getPreselectCondition(): Condition<AnAction> =
        Condition<AnAction> { if (it is SelectProblemAction) it.isSelected else false }

    override fun shouldShowDisabledActions(): Boolean = true

    private class SelectProblemAction(
        private val problem: SDProblem,
        val isSelected: Boolean
    ) : DumbAwareAction() {
        init {
            val name = (if (problem.solved) "✅ " else "") + problem.name
            templatePresentation.setText(name, false)
            templatePresentation.icon = ColorIcon(13, problem.rgb)
        }

        override fun getActionUpdateThread(): ActionUpdateThread = ActionUpdateThread.BGT

        override fun actionPerformed(e: AnActionEvent) {
            SDom.currentProblem = problem
        }
    }
}

class SDProblemSelectionNotificationAction(text: String) : NotificationAction(text) {
    constructor() : this("")

    private val selectionAction = SDProblemSelectionComboBoxAction()
    override fun actionPerformed(e: AnActionEvent, notification: Notification) = selectionAction.actionPerformed(e)
}