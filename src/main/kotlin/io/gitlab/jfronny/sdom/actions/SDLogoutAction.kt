package io.gitlab.jfronny.sdom.actions

import com.intellij.openapi.actionSystem.ActionUpdateThread
import com.intellij.openapi.actionSystem.AnActionEvent
import com.intellij.openapi.project.DumbAwareAction
import io.gitlab.jfronny.sdom.SDom

class SDLogoutAction : DumbAwareAction() {
    override fun actionPerformed(e: AnActionEvent) = SDom.logout()

    override fun update(e: AnActionEvent) {
        e.presentation.isVisible = SDom.loggedIn
    }

    override fun getActionUpdateThread(): ActionUpdateThread = ActionUpdateThread.BGT
}