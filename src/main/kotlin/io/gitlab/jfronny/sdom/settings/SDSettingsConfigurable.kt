package io.gitlab.jfronny.sdom.settings

import com.intellij.openapi.options.Configurable
import javax.swing.JComponent

class SDSettingsConfigurable : Configurable {
    private var settingsComponent: SDSettingsComponent? = null
    override fun getDisplayName(): String = "S-dom"
    override fun getPreferredFocusedComponent(): JComponent? {
        return settingsComponent!!.pdfModeBox
    }

    override fun createComponent(): JComponent? {
        val component = SDSettingsComponent()
        settingsComponent = component
        return component.mainPanel
    }

    override fun isModified(): Boolean {
        val state = SDSettings.getInstance().state
        return settingsComponent!!.pdfModeBox.selectedItem != state.pdfMode
    }

    override fun apply() {
        val state = SDSettings.getInstance().state
        state.pdfMode = settingsComponent!!.pdfModeBox.selectedItem as PdfMode
    }

    override fun reset() {
        val state = SDSettings.getInstance().state
        settingsComponent!!.pdfModeBox.selectedItem = state.pdfMode
    }

    override fun disposeUIResources() {
        settingsComponent = null
    }
}