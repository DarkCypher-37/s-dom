package io.gitlab.jfronny.sdom.settings

import com.intellij.openapi.application.ApplicationManager
import com.intellij.openapi.components.*

@Service
@State(name = "S-DOM", storages = [Storage("s-dom.xml")])
class SDSettings : SimplePersistentStateComponent<SDSettings.SDState>(SDState()) {
    class SDState : BaseState() {
        var url by string("https://domjudge.iti.kit.edu/main/api/v4")
        var pdfMode by enum<PdfMode>(PdfMode.INTERNAL_MEMORY)
    }

    companion object {
        fun getInstance(): SDSettings = ApplicationManager.getApplication().getService(SDSettings::class.java)
    }
}