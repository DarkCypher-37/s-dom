package io.gitlab.jfronny.sdom.settings

import com.intellij.credentialStore.CredentialAttributes
import com.intellij.credentialStore.Credentials
import com.intellij.credentialStore.generateServiceName
import com.intellij.ide.passwordSafe.PasswordSafe
import com.intellij.openapi.application.ApplicationManager

object SDCredentials {
    private fun createCredentialAttributes(name: String): CredentialAttributes {
        return CredentialAttributes(generateServiceName("s-dom", name))
    }

    var credentials: Pair<String?, String?>
        get() = PasswordSafe.instance[createCredentialAttributes("httpAuth")]
            .run { this?.userName to this?.getPasswordAsString() }
        set(value) {
            PasswordSafe.instance[createCredentialAttributes("httpAuth")] = Credentials(value.first, value.second)
            loggedIn = value.first != null && value.second != null
        }

    fun logOut() {
        PasswordSafe.instance[createCredentialAttributes("httpAuth")] = null
        PasswordSafe.instance[createCredentialAttributes("teamId")] = null
        loggedIn = false
    }

    var url: String
        get() = SDSettings.getInstance().state.url ?: "https://domjudge.iti.kit.edu/main/api/v4"
        set(value) {
            SDSettings.getInstance().state.url = value
        }

    var teamId: String?
        get() = PasswordSafe.instance[createCredentialAttributes("teamId")]?.userName
        set(value) {
            PasswordSafe.instance[createCredentialAttributes("teamId")] = Credentials(value)
        }

    var loggedIn: Boolean = credentials.first != null && credentials.second != null
        private set
}