package io.gitlab.jfronny.sdom.settings

enum class PdfMode(private val visual: String) {
    INTERNAL_MEMORY("Internal viewer, in-memory"),
    INTERNAL_FILE("Internal viewer, as file"),
    EXTERNAL_FILE("System-default");

    override fun toString(): String {
        return visual
    }
}