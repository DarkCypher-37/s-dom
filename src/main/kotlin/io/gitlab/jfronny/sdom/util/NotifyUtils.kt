package io.gitlab.jfronny.sdom.util

import com.intellij.notification.NotificationGroupManager
import com.intellij.notification.NotificationType
import com.intellij.openapi.actionSystem.AnAction
import com.intellij.openapi.project.Project

fun notify(context: Project?, title: String, content: String, notificationType: NotificationType = NotificationType.ERROR, vararg actions: AnAction) {
    NotificationGroupManager.getInstance()
        .getNotificationGroup("sdom.notifications")
        .createNotification(content, notificationType)
        .setTitle(title)
        .apply { actions.forEach { addAction(it) } }
        .notify(context)
}
