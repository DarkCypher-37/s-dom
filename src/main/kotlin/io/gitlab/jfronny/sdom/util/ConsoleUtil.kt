package io.gitlab.jfronny.sdom.util

import com.intellij.execution.ui.ConsoleView
import com.intellij.execution.ui.ConsoleViewContentType

enum class OutputType {
    ERROR, SUCCESS, OUTPUT
}

fun ConsoleView.println(text: String, contentType: OutputType) {
    print(
        "${text}\n", when (contentType) {
            OutputType.ERROR -> ConsoleViewContentType.ERROR_OUTPUT
            OutputType.SUCCESS -> ConsoleViewContentType.USER_INPUT
            OutputType.OUTPUT -> ConsoleViewContentType.NORMAL_OUTPUT
        }
    )
}