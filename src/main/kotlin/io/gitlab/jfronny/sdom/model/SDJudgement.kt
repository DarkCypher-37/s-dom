package io.gitlab.jfronny.sdom.model

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class SDJudgement(
    @SerialName("start_time") val startTime: String?,
    @SerialName("start_contest_time") val startContestTime: String,
    @SerialName("end_time") val endTime: String?,
    @SerialName("end_contest_time") val endContestTime: String?,
    @SerialName("submission_id") val submissionId: String,
    val id: String,
    val valid: Boolean,
    @SerialName("judgement_type_id") val judgementTypeId: String?,
    // not needed @SerialName("judgehost") val judgeHost: String?,
    @SerialName("max_run_time") val maxRunTime: Float?,
)