package io.gitlab.jfronny.sdom.model.scoreboard

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class ScoreboardRow(
    val rank: Int,
    @SerialName("team_id") val teamId: String,
    val score: Score,
    val problems: List<ScoreboardProblem>
)
