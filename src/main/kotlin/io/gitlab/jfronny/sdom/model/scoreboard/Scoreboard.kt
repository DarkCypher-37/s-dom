package io.gitlab.jfronny.sdom.model.scoreboard

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class Scoreboard (
    @SerialName("event_id") val eventId: String,
    val time: String,
    @SerialName("contest_time") val contestTime: String,
    val state: ContestState,
    val rows: List<ScoreboardRow>,
)