package io.gitlab.jfronny.sdom.model

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class SDLanguage(
    @SerialName("compile_executable_hash") val compileExecutableHash: String?,
    val id: String,
    val name: String,
    val extensions: List<String>,
    @SerialName("filter_compiler_files") val filterCompilerFiles: Boolean,
    @SerialName("allow_judge") val allowJudge: Boolean,
    @SerialName("time_factor") val timeFactor: Double,
    @SerialName("entry_point_required") val entryPointRequired: Boolean,
    @SerialName("entry_point_name") val entryPointName: String?,
)