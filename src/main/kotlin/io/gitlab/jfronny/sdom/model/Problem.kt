package io.gitlab.jfronny.sdom.model

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class Problem(
    val id: String,
    @SerialName("short_name") val shortName: String,
    val rgb: String,
    val color: String,
    val label: String,
    @SerialName("time_limit") val timeLimit: Float,
    val statement: List<FileWithName>,
    @SerialName("externalid") val externalId: String,
    val name: String,
)