package io.gitlab.jfronny.sdom.model

import kotlinx.serialization.Serializable

@Serializable
data class ImageFile(
    val href: String,
    val mime: String,
    val filename: String,
    val width: Int,
    val height: Int,
)