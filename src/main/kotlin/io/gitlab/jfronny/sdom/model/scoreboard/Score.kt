package io.gitlab.jfronny.sdom.model.scoreboard

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class Score(
    @SerialName("num_solved") val numSolved: Int,
    //not needed here @SerialName("total_time") val totalTime: Int,
)
