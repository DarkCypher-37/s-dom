package io.gitlab.jfronny.sdom.model.scoreboard

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class ContestState(
    val started: String?,
    val ended: String?,
    val frozen: String?,
    val thawed: String?,
    val finalized: String?,
    @SerialName("end_of_updates") val endOfUpdates: String?,
)
