package io.gitlab.jfronny.sdom

import com.intellij.ide.util.PropertiesComponent
import com.intellij.notification.NotificationType
import com.intellij.openapi.actionSystem.*
import com.intellij.openapi.application.ApplicationManager
import com.intellij.openapi.diagnostic.LogLevel
import com.intellij.openapi.diagnostic.Logger
import com.intellij.openapi.project.Project
import io.gitlab.jfronny.sdom.actions.SDGetContestsAction
import io.gitlab.jfronny.sdom.actions.SDGetProblemsAction
import io.gitlab.jfronny.sdom.model.*
import io.gitlab.jfronny.sdom.model.scoreboard.Scoreboard
import io.gitlab.jfronny.sdom.settings.SDCredentials
import io.gitlab.jfronny.sdom.util.notify
import io.ktor.client.*
import io.ktor.client.call.*
import io.ktor.client.engine.java.*
import io.ktor.client.plugins.auth.*
import io.ktor.client.plugins.auth.providers.*
import io.ktor.client.plugins.contentnegotiation.*
import io.ktor.client.request.*
import io.ktor.http.*
import io.ktor.serialization.kotlinx.json.*
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.SharedFlow
import kotlinx.serialization.json.Json
import java.io.ByteArrayOutputStream
import java.util.*
import java.util.zip.ZipEntry
import java.util.zip.ZipOutputStream

typealias ResultFlowListener = (SharedFlow<Result<SDJudgement>>, Contest, SDProblem, String) -> Unit

object SDom {
    private const val CONTEST_ID_PROPERTY = "io.gitlab.jfronny.sdom.contestId"

    private val logger = Logger.getInstance(SDom.javaClass).apply { setLevel(LogLevel.DEBUG) }
    private var propertiesComponent: PropertiesComponent? = null

    private val client = HttpClient(Java) {
        engine {
            protocolVersion = java.net.http.HttpClient.Version.HTTP_2
        }
        install(ContentNegotiation) {
            json(Json {
                ignoreUnknownKeys = true
            })
        }
    }

    private val authenticatedClient = HttpClient(Java) {
        engine {
            protocolVersion = java.net.http.HttpClient.Version.HTTP_2
        }
        install(ContentNegotiation) {
            json(Json {
                ignoreUnknownKeys = true
            })
        }
        install(Auth) {
            basic {
                credentials {
                    BasicAuthCredentials(SDCredentials.credentials.first ?: "", SDCredentials.credentials.second ?: "")
                }
                sendWithoutRequest { true }
            }
        }
    }

    private val loginListeners: MutableList<() -> Unit> = mutableListOf({
        SDGetContestsAction().actionPerformed(
            AnActionEvent(
                null,
                DataContext.EMPTY_CONTEXT,
                ActionPlaces.UNKNOWN,
                Presentation(),
                ActionManager.getInstance(),
                0
            )
        )
    })
    private val logoutListeners: MutableList<() -> Unit> = mutableListOf()

    private val resultFlowListeners: MutableList<ResultFlowListener> = mutableListOf()

    fun registerLoginListener(listener: () -> Unit) {
        loginListeners.add(listener)
    }

    fun registerLogoutListener(listener: () -> Unit) {
        logoutListeners.add(listener)
    }

    fun registerResultFlowListener(listener: ResultFlowListener) {
        resultFlowListeners.add(listener)
    }

    suspend fun login(username: String, password: String, url: String) {
        val fixedApi = url.trimEnd('/').run { if (endsWith("/api/v4")) this else "$this/api/v4" }
        val result: SDLoginResult = client.get(fixedApi) {
            url {
                appendPathSegments("user")
            }
            logger.info("Using API: ${this.url}")
            basicAuth(username, password)
        }.body()

        if (!result.enabled) throw Exception("User is not enabled")
        SDCredentials.credentials = Pair(username, password)
        SDCredentials.teamId = result.teamId
        SDCredentials.url = fixedApi

        loginListeners.forEach { ApplicationManager.getApplication().invokeLater(it) }
        logger.debug("Logged in as $username")
    }

    fun logout() {
        SDCredentials.logOut()
        logoutListeners.forEach { ApplicationManager.getApplication().invokeLater(it) }
    }

    var contests: List<Contest> = listOf()
    var currentContest: Contest? = null
        set(value) {
            field = value
            propertiesComponent?.setValue(CONTEST_ID_PROPERTY, value?.id, "")
        }

    var problems: Map<String, SDProblem>? = null
    var currentProblem: SDProblem? = null
    var judgementTypes: Map<String, SDJudgementType>? = null
    private var languages: Map<String, SDLanguage>? = null

    suspend fun getContests() {
        val result = authenticatedClient.get(SDCredentials.url) {
            url {
                appendPathSegments("contests")
            }
        }
        if (result.status.isSuccess()) {
            contests = result.body<List<Contest>>()

            logger.warn(contests.toString())
        }
    }

    fun loadProperties(project: Project?) {
        propertiesComponent = project?.let { PropertiesComponent.getInstance(it) } ?: PropertiesComponent.getInstance()
        val contestId = propertiesComponent?.getValue(CONTEST_ID_PROPERTY, "")?.takeIf { it != "" }
        val contest = contests.find { it.id == contestId }
        if (contestId != null && contest != null) {
            CoroutineScope(Job() + Dispatchers.IO).launch {
                selectContest(contest, project)
            }
        }
    }

    suspend fun loadProblems() {
        fetchProblems()
        getSolvedProblems()
    }

    private suspend fun fetchProblems() {
        val contest = currentContest ?: return
        val result = authenticatedClient.get(SDCredentials.url) {
            url {
                appendPathSegments("contests", contest.id, "problems")
            }
        }
        if (result.status.isSuccess()) {
            problems = result.body<List<Problem>>()
                .map { SDProblem.of(it, false) }
                .associateBy { it.id }

            if (problems?.containsValue(currentProblem) != true) {
                currentProblem = null
            }
        }
    }

    private suspend fun getSolvedProblems() {
        val teamId = SDCredentials.teamId ?: return
        val contest = currentContest ?: return
        val result = authenticatedClient.get(SDCredentials.url) {
            url {
                appendPathSegments("contests", contest.id, "scoreboard")
            }
        }
        if (result.status.isSuccess()) {
            val scoreboard = result.body<Scoreboard>()
            val solvedProblems = scoreboard.rows
                .firstOrNull { it.teamId == teamId }
                ?.let { row ->
                    row.problems.associateBy { it.problemId }
                }

            problems?.values?.forEach { it.solved = solvedProblems?.get(it.id)?.solved ?: false }
        }
    }

    suspend fun selectContest(contest: Contest, context: Project?) {
        currentContest = contest

        SDGetProblemsAction().actionPerformed(
            AnActionEvent(
                null,
                DataContext.EMPTY_CONTEXT,
                ActionPlaces.UNKNOWN,
                Presentation(),
                ActionManager.getInstance(),
                0
            )
        )

        try {
            judgementTypes = authenticatedClient.get(SDCredentials.url) {
                    url {
                        appendPathSegments("contests", contest.id, "judgement-types")
                    }
                }
                .body<List<SDJudgementType>>()
                .associateBy { it.id }

            languages = authenticatedClient.get(SDCredentials.url) {
                    url {
                        appendPathSegments("contests", contest.id, "languages")
                    }
                    contentType(ContentType.Application.Json)
                }
                .body<List<SDLanguage>>()
                .flatMap { l -> l.extensions.map { it to l } }
                .toMap()
        } catch (e: Exception) {
            notify(context,
                "Unable to load data",
                "Unable to get judgement types and supported languages for the contest",
                NotificationType.WARNING
            )
            return
        }
    }

    suspend fun downloadProblemStatement(contest: Contest, problem: SDProblem): ByteArray {
        val response: ByteArray = authenticatedClient.get(SDCredentials.url) {
            url {
                appendPathSegments("contests", contest.id, "problems", problem.id, "statement")
            }
            contentType(ContentType.Application.Pdf)
        }.body()

        return response
    }

    fun identifyLanguage(extension: String): SDLanguage? {
        return languages?.get(extension)
    }

    suspend fun submitSolution(
        contest: Contest, problem: SDProblem, solution: String, fileName: String, language: SDLanguage
    ): SharedFlow<Result<SDJudgement>> = coroutineScope {
        val resultFlow: MutableSharedFlow<Result<SDJudgement>> = MutableSharedFlow()

        launch {
            try {
                val response: SDSubmission = authenticatedClient.post(SDCredentials.url) {
                    url {
                        appendPathSegments("contests", contest.id, "submissions")
                    }
                    contentType(ContentType.Application.Json)
                    setBody(
                        SDAddSubmission(
                            problem = problem.id,
                            problemId = problem.id,
                            language = language.id,
                            languageId = language.id,
                            entryPoint = null,
                            files = listOf(SDAddSubmissionFile(zip(fileName, solution)))
                        )
                    )
                }.body()

                logger.info(response.toString())
//                if (response.importError != null) {
//                    resultFlow.emit(Result.failure(Exception(response.importError)))
//                    return@launch
//                }
                do {
                    val result: List<SDJudgement> = authenticatedClient.get(SDCredentials.url) {
                        url {
                            appendPathSegments("contests", contest.id, "judgements")
                            parameters.append("submission_id", response.id)
                        }
                    }.body()
                    if (result.isNotEmpty()) {
                        result.forEach { resultFlow.emit(Result.success(it)) }
                        break
                    } else {
                        delay(1000)
                    }
                } while (true)
            } catch (e: Exception) {
                logger.error("Failed to submit solution", e)
                resultFlow.emit(Result.failure(e))
            }
        }
        resultFlowListeners.forEach { it -> ApplicationManager.getApplication().invokeLater { it(resultFlow, contest, problem, fileName) } }
        return@coroutineScope resultFlow
    }

    private fun zip(name: String, content: String): String =
        String(Base64.getEncoder().encode(ByteArrayOutputStream().use { baos ->
            ZipOutputStream(baos).use {
                it.putNextEntry(ZipEntry(name))
                it.write(content.toByteArray())
                it.closeEntry()
            }
            baos.toByteArray()
        }))

    val loggedIn: Boolean get() = SDCredentials.loggedIn

    init {
        if (loggedIn) {
            loginListeners.forEach { ApplicationManager.getApplication().invokeLater(it) }
        }
    }
}