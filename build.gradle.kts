import io.gitlab.jfronny.scripts.*

plugins {
    id("java")
    id("org.jetbrains.intellij") version "1.17.3"
    kotlin("jvm") version "1.9.22"
    kotlin("plugin.serialization") version "1.9.22"
    id("jf.autoversion") version "1.6-SNAPSHOT"
    id("de.undercouch.download") version "5.3.0"
}

group = "io.gitlab.jfronny"

repositories {
    mavenCentral()
}

// Configure Gradle IntelliJ Plugin
// Read more: https://plugins.jetbrains.com/docs/intellij/tools-gradle-intellij-plugin.html
intellij {
    version.set("2024.1.1")
    type.set("CL") // Target IDE Platform

    plugins.set(listOf(/* Plugin Dependencies */))
}

dependencies {
    implementation("io.ktor:ktor-client-core:2.3.11")
    implementation("io.ktor:ktor-client-java:2.3.11")
    implementation("io.ktor:ktor-client-auth:2.3.11")
    implementation("io.ktor:ktor-client-content-negotiation:2.3.11")
    implementation("io.ktor:ktor-serialization-kotlinx-json:2.3.11")

    implementation("org.jetbrains.kotlinx:kotlinx-serialization-json:1.6.3")
}

tasks {
    val extraResources = layout.buildDirectory.dir("downloads/resources")
    val pdfJsVersion = "3.11.174"
    val downloadPdfViewerCss by registering(de.undercouch.gradle.tasks.download.Download::class) {
        src("https://cdnjs.cloudflare.com/ajax/libs/pdf.js/$pdfJsVersion/pdf_viewer.min.css")
        dest(extraResources.map { it.file("pdf.viewer.css") })
        overwrite(false)
    }
    val downloadPdfViewerJs by registering(de.undercouch.gradle.tasks.download.Download::class) {
        src("https://cdnjs.cloudflare.com/ajax/libs/pdf.js/$pdfJsVersion/pdf.min.js")
        dest(extraResources.map { it.file("pdf.mjs") })
        overwrite(false)
    }
    val downloadPdfWorkerJs by registering(de.undercouch.gradle.tasks.download.Download::class) {
        src("https://cdnjs.cloudflare.com/ajax/libs/pdf.js/$pdfJsVersion/pdf.worker.min.js")
        dest(extraResources.map { it.file("pdf.worker.mjs") })
        overwrite(false)
    }

    processResources {
        dependsOn(downloadPdfViewerCss, downloadPdfViewerJs, downloadPdfWorkerJs)
    }

    sourceSets.main {
        resources.srcDirs(extraResources)
    }

    // Set the JVM compatibility versions
    withType<JavaCompile> {
        sourceCompatibility = "17"
        targetCompatibility = "17"
    }
    withType<org.jetbrains.kotlin.gradle.tasks.KotlinCompile> {
        kotlinOptions.jvmTarget = "17"
    }

    patchPluginXml {
        sinceBuild = "241"
        untilBuild = "242.*"
        changeNotes = changelogHtml
    }

    signPlugin {
        certificateChain = System.getenv("CERTIFICATE_CHAIN")
        privateKey = System.getenv("PRIVATE_KEY")
        password = System.getenv("PRIVATE_KEY_PASSWORD")
    }

    publishPlugin {
        token = System.getenv("PUBLISH_TOKEN")
    }

    run {
        // workaround for https://youtrack.jetbrains.com/issue/IDEA-285839/Classpath-clash-when-using-coroutines-in-an-unbundled-IntelliJ-plugin
        buildPlugin {
            exclude { "coroutines" in it.name }
        }
        prepareSandbox {
            exclude { "coroutines" in it.name }
        }
    }
}
