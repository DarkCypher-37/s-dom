# S-DOM
S-DOM is a plugin for IntelliJ IDEA that allows you to submit your code to the DOMjudge system directly from the IDE.
It isn't a full replacement for the web interface yet, but already matches or surpasses the web UI in most tasks.

You can download the plugin from the [JetBrains Marketplace](https://plugins.jetbrains.com/plugin/24446-s-dom-for-domjudge).

## Features
- Simple, one-click submissions and feedback (no need for File Explorer or reloading!)
- View problems and problem statements without leaving your IDE
- Unobtrusive and integrated
- Detects the language of submitted files

<!-- Note to writers: A copy of the feature list exists in plugin.xml, please also add your changes there -->

## Potential future features
- Display how much time is left in a contest or whether it's already over
- Implement reading detailed results
- Show scoreboards
- Maybe even download test cases and scaffold sources

## Contributing
If you want to contribute, feel free to fork the [GitLab repository](https://gitlab.com/JFronny/s-dom) and submit a merge request there.
I'm happy to accept any help, be it bug fixes, new features, or just code cleanup.
Please note that I will merge MRs locally, so don't be surprised if your MR is marked as closed without merging.
Also note that S-dom uses Gradle to build but does not use the gradle wrapper, so you should have Gradle installed on your system.
Also take a look at the references below for some resources for developing this plugin.

## References
- [DOMjudge](https://www.domjudge.org/documentation)
- [DOMjudge API](https://domjudge.iti.kit.edu/main/api/doc)
- [SimpleCodeTester plugin](https://github.com/Mr-Pine/SimpleCodeTester-IntelliJ-Plugin)
- [Example Route](https://domjudge.iti.kit.edu/main/api/v4/contests/5/problems)
- [All IDEA UI Icons](https://jetbrains.design/intellij/resources/icons_list/)

## Publishing
Note: This section is only relevant for the maintainer of this repository and provided mostly for reference.

- To publish a new version, first create a new tag for that version and push it to the repository.
- To do so, use the `bumpVersion` task provided by `jf.autoversion` ([source code](https://git.frohnmeyer-wds.de/Johannes/Scripts)) by running `gradle bumpVersion -PnextVersionType=release`.
- Then push the tag to the repository using `git push --tags`.
- After that, run `gradle buildPlugin -Prelease` to build the plugin. You can find the output of that task as a zip file in the `build/distributions` directory.
- Finally, upload the zip file to the JetBrains Marketplace.